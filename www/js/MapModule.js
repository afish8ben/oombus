/*global oombus */
if (!this.oombus || typeof this.oombus !== 'object') {
    this.oombus = {};
}
//==================================
// MapModule
//
// For info on working with the map go to: http://leafletjs.com/
//==================================
(function () {
    'use strict';
    oombus.Viewer.module('MapModule', function (Mod, Viewer, Backbone, Marionette, $, _) {

        //==================================
        //initializer called on Viewer.start(options)
        //==================================
        Mod.addInitializer(function (options) {
            Mod.controller = new Controller({
            		config:options,
            		region: Viewer.mapRegion
            	});
        });
        //==================================
        //Controller for the Module
        //==================================
        var Controller = Backbone.Marionette.Controller.extend({
            initialize: function (options) {
                _.bindAll(this, 'showSidebar', 'showUserLocation', 'centerAt', 'placeBuses');
                this.config = options.config;
                this.region = options.region;
                //setup the map here...

                //hook up App events
                Viewer.vent.on('Map:WorkItemsLayer:DataChanged', this.updateWorkItemLayer, this);            
                Viewer.vent.on('Map:CenterAt', this.centerAt, this);
                Viewer.vent.on('Map:SetBasemap',this.setBaseMap,this);

                //set the show worklist event from the navItem config eventToRaise
                Viewer.vent.on('View:RouteList', this.showSidebar);

                //catch the show user location event from the NavbarModule
                Viewer.vent.on('Map:ShowUserLocation', this.showUserLocation)
                Viewer.vent.on('Map:PlotData', this.placeBuses);

                this.initMap(this.config.mapConfig);
            },
            centerAt:function(position){
            	console.log('Map:CenterAt Caught');
            	console.dir(position);
            	var pt = L.latLng(position.coords.latitude, position.coords.longitude);
            	
                //todo: this doesn't work. we need to set the zoom level to 10 (?) if the user is zoomed to far in or out
                if(this.map.getZoom() < 10){
            		this.map.panTo(pt,10);
		        }else{
		        	this.map.panTo(pt);
		        }
            	
                var marker = L.marker(pt).addTo(this.map);
                marker.bindPopup("<b>Current Location</b><br>You are here.");
            },
            placeBuses:function(){
				console.log(Viewer.BusRoutesModule.data);
                if(Viewer.BusRoutesModule.data){
                    var points = [];
                    _.each(Viewer.BusRoutesModule.data, function(bus){
                        points.push({
                            lat: bus.LATITUDE, long: bus.LONGITUDE,
                            location: bus.TIMEPOINT, direction: bus.DIRECTION
                        });
                    })

                    var busIcon = L.icon({
                        iconUrl:'../www/img/busIcon.png',
                        iconRetinaUrl: '../www/img/busIcon.png',
                        iconSize:[15, 30],
                        iconAnchor:[10, 15],
                        popupAnchor:[-3, -10]
                    });

                    for(var i = 0; i < points.length; i++){
                        var pt = L.latLng(points[i].lat, points[i].long);
                        var marker = L.marker(pt, {icon:busIcon}).addTo(this.map);
                        marker.bindPopup(points[i].direction + " from " + points[i].location);
                    }

                }else{
                    console.log("No Data");
                }
            },
            showSidebar:function() {
                var template = "<ul class='routes'>";
                var buses = Viewer.BusRoutesModule.data;
                _.each(buses, function(bus){
                   template += "<li>"+bus.VEHICLE + ": " + bus.DIRECTION + " from " + bus.TIMEPOINT +"</li>";
                });
                template += "</ul>";
                this.sidebar.setContent(template);
                this.sidebar.toggle();
            },
            showUserLocation:function() {
                //todo: get user location, add pushpin, and zoom to location
                console.log('get user location event caught');

                if (navigator.geolocation) {
                    console.log(this);
                    navigator.geolocation.getCurrentPosition(this.centerAt);
                }else if(window.navigator){ //Using cordova libraries
                    window.navigator.geolocation.getCurrentPosition(this.centerAt);
                }
                else {
                    console.log('Geolocation is not supported by this browser.');
                }
            },
            initMap: function (mapConfig) {
                console.log('Map:Controller:initMap');

                /* Basemap Layers */
                // TODO: update this to use initBaseLayers method
                var mapquestOSM = L.tileLayer(mapConfig.url, {
                  maxZoom: 19,
                  subdomains: mapConfig.subdomains,
                  attribution: mapConfig.attribution
                });
                
                this.map = L.map("map", {
                  zoom: mapConfig.zoom,
                  center: mapConfig.center,
                  layers: [mapquestOSM]
                });

                this.sidebar = L.control.sidebar("sidebar", {
                  closeButton: true,
                  position: "left"
                }).addTo(this.map);

                this.showUserLocation();
            },
            // this method is not currently used ---------
            onMapLoad: function (map) {
                $(window).resize(this.resizeMap);
            }

        });
    });
})();