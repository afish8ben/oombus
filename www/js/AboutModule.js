/*global mapwrx */
if (!this.mapwrx || typeof this.mapwrx !== 'object') {
    this.mapwrx = {};
}
(function () {
    'use strict';
    mapwrx.Viewer.module('AboutModule', function (Mod, Viewer, Backbone, Marionette, $, _) {

        //==================================
        //initializer called on Viewer.start(options)
        //==================================
        Mod.addInitializer(function (options) {
            Mod.controller = new Controller({
                //we pass in the region from the app because it will be
                //converted into a Marionette.Region for us
                region: Viewer.aboutRegion
            });
        });


        //==================================
        //Controller for the LoginModule
        //==================================
        var Controller = Backbone.Marionette.Controller.extend({
            initialize: function (options) {
                _.bindAll(this, 'logoutSuccess', 'loginSuccess');
                this.region = options.region;
                console.log('LoginModule:Controller:initialize');

                this.layout = new LoginLayout();
                this.region.show(this.layout);

                //set the show worklist event from the navItem config eventToRaise
                Viewer.vent.on('View:Login', this.loginOrLogout);

                // try to get the user session
                $.ajax({
                    url: "/user",
                    type: "GET",
                    success: this.loginSuccess,
                    error: this.loginFailed,
                    complete: this.loginComplete
                })
            },
            login: function (email, password) {

                //todo: remove this for PROD!!!!!!!!!!!!!
                console.log('email: ' + email);
                console.log('password: ' + password);

                //show spinner on submit button
                //call login service route

                /* old way
                $(".form-signin").submit(function(e){
                    $.ajax({
                        url: "/login",
                        type: "POST",
                        data: $(this).serialize(),
                        success: function(data){
                          console.log(data);
                          $('#loginLink').html('Logout');
                        },
                        error: function(e){
                          console.log(e.responseText);
                          if (e.responseText === 'user-not-found') {
                            // alert the user they entered invalid credentials

                          }
                        }
                    });
                    return false;
                });
                */

                $.ajax({
                    url: "/login",
                    type: "POST",
                    data: {
                        email: email,
                        password: password,
                        remember: true
                    },
                    dataType: 'json',
                    success: this.loginSuccess,
                    error: this.loginFailed,
                    complete: this.loginComplete
                });
            },
            loginOrLogout: function() {
                if (mapwrx.user == null) {
                    console.log('showModal called');
                    $('#loginModal').modal('show');
                } else {
                    $.ajax({
                        url: "/logout",
                        type: "GET",
                        dataType: 'json',
                        success: function(data) {
                            console.log('logout success called' + data);
                            Viewer.vent.trigger('Login:LogoutSuccess');
                        }
                    });
                    mapwrx.user = null;
                }
            },
            loginSuccess: function(user) {
                //set the mapwrx user object
                mapwrx.user = user;
                if (!user.updatePassword) {
                    Viewer.vent.trigger('Login:LoginSuccess');
                    $('#loginModal').modal('hide');
                }  else {
                    //todo: show update password input boxes and change 'Sign in' text to 'Submit' now
                    Viewer.vent.trigger('Login:LoginSuccess');
                    $('#loginModal').modal('hide');
                }
            },
            loginFailed: function() {
                $('#email-address').addClass('error');
                Viewer.vent.trigger('Login:LoginFailed'); 
            },
            loginComplete: function() {
                // hide the spinner
            },
            logoutSuccess: function(data) {
                console.log('logout success called');
                Viewer.vent.trigger('Login:LogoutSuccess');
            }
        });

        var LoginLayout = Backbone.Marionette.Layout.extend({
            template: '#login-template',
            events: {
                'keypress':'submitOnEnter',
                'click #submit-button':'submit'
            },
            submitOnEnter:function(e){
                if(e.keyCode == 13){
                    this.submit();
                    return false;
                }
            },
            submit: function(){
                Mod.controller.login($('#email-address').val(), $('#password').val());
                return false;
            }
        });
    });
})();