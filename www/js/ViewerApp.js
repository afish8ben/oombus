﻿/*global oombus */
if (!this.oombus || typeof this.oombus !== 'object') {
    this.oombus = {};
}
(function () {
    'use strict';

    oombus.Viewer = new Backbone.Marionette.Application();

    oombus.Viewer.addRegions({
        navItemsRegion: '#nav-items-region',
        mapRegion: '#map',
        busRoutesRegion: '#bus-routes-region'
    });    

    //debugging so we can see events flying around
    oombus.Viewer.vent.on('all', function (evt, model) {
        console.log('oombus.Viewer DEBUG: Event Caught: ' + evt);
        if (model) {
            console.dir(model);
        }
    });
})();

