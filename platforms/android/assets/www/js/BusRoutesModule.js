/*global oombus */
if (!this.oombus || typeof this.oombus !== 'object') {
    this.oombus = {};
}
(function () {
    'use strict';
    oombus.Viewer.module('BusRoutesModule', function (Mod, Viewer, Backbone, Marionette, $, _) {

        //==================================
        //initializer called on Viewer.start(options)
        //==================================
        Mod.addInitializer(function (options) {
            Mod.controller = new Controller({
                //we pass in the region from the app because it will be
                //converted into a Marionette.Region for us
                config:options,
                region: Viewer.busRoutesRegion
            });
        });


        //==================================
        //Controller for the BusRoutesModule
        //==================================
        var Controller = Backbone.Marionette.Controller.extend({
            initialize: function (options) {
                //_.bindAll(this, 'logoutSuccess', 'loginSuccess');
                this.region = options.region;
                console.log('BusRoutesModule:Controller:initialize');

                this.layout = new BusRoutesLayout();
                this.region.show(this.layout);

                Viewer.vent.on("busroutes:getBus", this.getBusRoutes);
                //set the show worklist event from the navItem config eventToRaise
                //Viewer.vent.trigger('View:BusRoutesInit');
            },
            getBusRoutes: function () {

                //show spinner on submit button
                //call login service route

                //CROSS-DOMAIN-ACCESS NOT ALLOWED!

                $.getJSON("http://developer.itsmarta.com/BRDRestService/BRDRestService.svc/GetAllBus",{},this.gotBusRoutes);
                /*
                $.ajax({
                    url: "http://developer.itsmarta.com/BRDRestService/BRDRestService.svc/GetAllBus",
                    type: "GET",
                    data: {},
                    dataType: 'json',
                    success: this.gotBusRoutes,
                    error: this.serviceFailed,
                    complete: this.gotBusComplete
                });
                */
            },
            loginOrLogout: function() {
                if (oombus.user == null) {
                    console.log('showModal called');
                    $('#loginModal').modal('show');
                } else {
                    $.ajax({
                        url: "/logout",
                        type: "GET",
                        dataType: 'json',
                        success: function(data) {
                            console.log('logout success called' + data);
                            Viewer.vent.trigger('Login:LogoutSuccess');
                        }
                    });
                    oombus.user = null;
                }
            },
            gotBusRoutes: function(data) {
                //set the oombus user object
                oombus.data = data;
                Viewer.vent.trigger("Map:PlotData");
                //TODO do something with the data.
            },
            serviceFailed: function() {
                //TODO Fail gracefully
            },
            gotBusComplete: function() {
                // hide the spinner
            },
            logoutSuccess: function(data) {
                console.log('logout success called');
                Viewer.vent.trigger('Login:LogoutSuccess');
            }
        });

        var BusRoutesLayout = Backbone.Marionette.Layout.extend({
            template: '#bus-routes-template'
        });
    });
})();