/*global oombus */
if (!this.oombus || typeof this.oombus !== 'object') {
    this.oombus = {};
}
(function () {
    'use strict';
    oombus.Viewer.module('NavbarModule', function (Mod, Viewer, Backbone, Marionette, $, _) {

        //==================================
        //initializer called on Viewer.start(options)
        //==================================
        Mod.addInitializer(function (options) {
            Mod.controller = new Controller({
                //navItems: options.navItems,
                //we pass in the region from the app because it will be
                //converted into a Marionette.Region for us
                region: Viewer.navItemsRegion
            });
        });


        //==================================
        //Controller for the NavbarModule
        //==================================
        var Controller = Backbone.Marionette.Controller.extend({
            initialize: function (options) {
                //_.bindAll();
                this.region = options.region;
                console.log('NavbarModule:Controller:initialize');

                this.layout = new NavbarLayout();
                this.region.show(this.layout);

                //hook up App events                 
            }
        });

        var NavbarLayout = Backbone.Marionette.Layout.extend({
            template: '#nav-items-template',
            events: {
                'click #nav-location':'locationClicked',
                'click #nav-routelist': 'routeListClicked'
            },
            locationClicked: function(){
                //todo: get user location here and call a map event to show (or move) thier pushpin
                Viewer.vent.trigger('Map:ShowUserLocation');
            },
            routeListClicked: function(){
                //Viewer.vent.trigger('View:RouteList');
                Viewer.vent.trigger("busroutes:getBus");
            }
        });

    });
})();